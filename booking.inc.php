

<html>
<body>
<?php
    require("config.php");
    session_start();
    $user_id = $_SESSION['id'];
    $booking_fullname = $_POST['name'];
    $booking_contact = $_POST['contact'];
    $booking_date = $_POST['date_input'];
    $booking_time = $_POST['time_input'];
    $numberPerson = $_POST['num_people'];
    $name_cafe = $_POST['name_cafe'];

    $error_msg = "";
    // checking empty fillin
    if(empty(trim($booking_fullname))){
        $error_msg = "Please enter your name.";
    }

    if(empty(trim($booking_contact))){
        $error_msg = "Please enter your contact number.";
    }

    if(empty(trim($booking_date))){
        $error_msg = "Please enter your booking date.";
    }

    if(empty(trim($booking_time))){
        $error_msg = "Please enter your booking time.";
    }

    if(empty(trim($numberPerson))){
        $error_msg = "Please enter number of person.";
    }

    if(empty(trim($name_cafe))){
        $error_msg = "Please select your cafe.";
    }

    // if one of them is not empty then error 
    if(empty($error_msg)){
        $insertBooking =  mysqli_query($link,"INSERT INTO bookingform (user_id,booking_fullname,booking_contact,booking_date,booking_time,numberPerson,name_cafe)
        VALUES('$user_id','$booking_fullname','$booking_contact', '$booking_date', '$booking_time', '$numberPerson', '$name_cafe' )");

        header("location: status.php");


    }else{
        header("location: bookingform.php?error=1");
    }
    


?>
</body>
</html>