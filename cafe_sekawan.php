<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sekawan Social Space</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/icon.jpg" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fixed-top topbar-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start justify-content-between ">
      <i class="bi bi-phone d-flex align-items-center "><span>+082-2272 64240</span></i>
      <i class="bi bi-clock ms-4 d-lg-flex align-items-center me-auto"><span>Tue-Sun: 10:00 AM - 22:00 PM</span></i>
    </div>
  </section>
  <!-- style="position: absolute; right: 5.5em;" -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <div class="logo me-auto">
        <h1><a href="welcome.php" style="font-size: 0.7em;">GoFe</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="welcome.php">Home</a></li>
          <li><a class="nav-link scrollto" href="#menu">Reservasi</a></li>
          <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
		  <li><a class="nav-link scrollto" href="status.php">Status</a></li>
		  <li><a class="nav-link scrollto" href="logout.php">Log Out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
        
          <div class="carousel-item active" style="background: url(assets/img/sekawan/bggg.jpg);">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown"><span>Sekawan  </span>Social Space</h2>
                <p class="animate__animated animate__fadeInUp"></p>
                <div>
                    <a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

  
	 <!-- ======= Gallery Section ======= -->
     <section id="gallery" class="gallery">
      <div class="container-fluid">

        <div class="section-title">
          <h2>All about <span>Sekawan Social Space</span></h2>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/sekawan/1.jpg" class="gallery-lightbox">
                <img src="assets/img/sekawan/1.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
              <br>
              <p align="justify">Cafe yang sangat nyaman, memiliki ruang indoor dan outdoor. Pelayanan yang super cepat dan juga
              terdapat panggung untuk live music. Cafe ini sering mengadakan berbagai macam event, seperti event clothing dll.
              </p>
              Lokasi : 
              <p align="justify">Jl. Stadion Mini 1 No.1, Karangbawang, Purwokerto Kulon, Kec. Purwokerto Sel., Kabupaten Banyumas, Jawa Tengah 53141
              </p><h3><p align="center"><b>Sekawan Social Space :</b></p></h3>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/sekawan/2.jpg" class="gallery-lightbox">
                <img src="assets/img/sekawan/2.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Main course-</b></p></h5>
            <table>
              <tr>
                <td>Nasi Goreng Tomyam</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Chicken Katsu Teriyaki</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
              <tr>
                <td>Bebek Goreng Bali</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
                <td>Chicken Black Paper</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
              </tr>
                <td>Ayam Bakar Kampung (Dada/Paha)</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
              </tr>
                <td>Chicken Katsu Pedas Mani</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
              </tr>
                <td>Bakmi Goreng</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Snack-</b></p></h5>
            <table>
              <tr>
                <td>Potato Wedges</td>
                <td>...................................................</td>
                <td>15k</td>
              </tr>
              <tr>
                <td>Mendoan</td>
                <td>...................................................</td>
                <td>10k</td>
              </tr>
              <tr>
                <td>French Fries</td>
                <td>...................................................</td>
                <td>15k</td>
              </tr>
                <td>Onion Rings</td>
                <td>...................................................</td>
                <td>14k</td>
              </tr>
              </tr>
                <td>Churros</td>
                <td>...................................................</td>
                <td>14k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Drink-</b></p></h5>
            <table>
              <tr>
                <td>Hazelnut Latte</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
              <tr>
                <td>Vanilla Twisty</td>
                <td>...................................................</td>
                <td>27k</td>
              </tr>
              <tr>
                <td>Peppermint Lemon</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
                <td>Creamy Orchid Oolong</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
              </tr>
                <td>Caramel Machiato</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
              </tr>
                <td>Vanila Brown</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
            </table>
          </div>
        </div>

        <br>

      </div>
    </section><!-- End Gallery Section -->

    <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
</body>
</html>