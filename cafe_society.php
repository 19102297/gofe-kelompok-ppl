<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Society Cafe</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/icon.jpg" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fixed-top topbar-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start justify-content-between ">
      <i class="bi bi-phone d-flex align-items-center "><span>+082-2272 64240</span></i>
      <i class="bi bi-clock ms-4 d-lg-flex align-items-center me-auto"><span>Tue-Sun: 10:00 AM - 22:00 PM</span></i>
    </div>
  </section>
  <!-- style="position: absolute; right: 5.5em;" -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <div class="logo me-auto">
        <h1><a href="welcome.php" style="font-size: 0.7em;">GoFe</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="welcome.php">Home</a></li>
          <li><a class="nav-link scrollto" href="#menu">Reservasi</a></li>
          <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
		  <li><a class="nav-link scrollto" href="status.php">Status</a></li>
		  <li><a class="nav-link scrollto" href="logout.php">Log Out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
        
          <div class="carousel-item active" style="background: url(assets/img/society/1.jpg);">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown"><span>Society</span>Cafe</h2>
                <p class="animate__animated animate__fadeInUp"></p>
                <div>
                    <a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

  
	 <!-- ======= Gallery Section ======= -->
     <section id="gallery" class="gallery">
      <div class="container-fluid">

        <div class="section-title">
          <h2>All about <span>Society Cafe</span></h2>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/society/2.jpg" class="gallery-lightbox">
                <img src="assets/img/society/2.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
              <br>
              <p align="justify">Cafe yang sangat nyaman dengan menu yang bermacam-macam, dari minuman sampai makanan. 
              Cafe ini bisa dibilang instagramable, karena setiap sudutnya bisa untuk berfoto. 
              </p>
              Lokasi : 
              <p align="justify">Jl. Prof. Dr. Suharso Kelurahan No.9B, Arcawinangun, Kec. Purwokerto Timur, Kabupaten Banyumas.
              </p><h3><p align="center"><b>Society Menu :</b></p></h3>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/society/3.jpg" class="gallery-lightbox">
                <img src="assets/img/society/3.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Main course-</b></p></h5>
            <table>
              <tr>
                <td>Nasi Goreng Tomyam</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Sop Tahu Daging Sapi</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Bebek Goreng Bali</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
                <td>Ayam Betutu Bakar</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
              </tr>
                <td>Ayam Panggang Hatty</td>
                <td>...................................................</td>
                <td>29k</td>
              </tr>
              </tr>
                <td>Iga Bakar 1 Tulang</td>
                <td>...................................................</td>
                <td>44k</td>
              </tr>
              </tr>
                <td>Sop Iga Bali 2 Tulang</td>
                <td>...................................................</td>
                <td>78k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Snack-</b></p></h5>
            <table>
              <tr>
                <td>Potato Wedges</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Roti Naan Susu/Gula Aren</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Roti Naan Keju</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
                <td>Tahu Tiga Rasa</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
              </tr>
                <td>Salad Mangga Muda</td>
                <td>...................................................</td>
                <td>29k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Drink-</b></p></h5>
            <table>
              <tr>
                <td>Tropical Lychee Berry</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Chamomile Lullaby</td>
                <td>...................................................</td>
                <td>45k</td>
              </tr>
              <tr>
                <td>Peppermint Lemon</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
                <td>Creamy Orchid Oolong</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
              </tr>
                <td>British Breakfast Tea</td>
                <td>...................................................</td>
                <td>29k</td>
              </tr>
              </tr>
                <td>Aoihana White Tea</td>
                <td>...................................................</td>
                <td>29k</td>
              </tr>
            </table>
          </div>
        </div>

        <br>

      </div>
    </section><!-- End Gallery Section -->

    <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
</body>
</html>