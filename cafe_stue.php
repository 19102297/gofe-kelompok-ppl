<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Stue Coffe</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/icon.jpg" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fixed-top topbar-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start justify-content-between ">
      <i class="bi bi-phone d-flex align-items-center "><span>+082-2272 64240</span></i>
      <i class="bi bi-clock ms-4 d-lg-flex align-items-center me-auto"><span>Tue-Sun: 10:00 AM - 22:00 PM</span></i>
    </div>
  </section>
  <!-- style="position: absolute; right: 5.5em;" -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <div class="logo me-auto">
        <h1><a href="welcome.php" style="font-size: 0.7em;">GoFe</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="welcome.php">Home</a></li>
          <li><a class="nav-link scrollto" href="#menu">Reservasi</a></li>
          <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
		  <li><a class="nav-link scrollto" href="status.php">Status</a></li>
		  <li><a class="nav-link scrollto" href="logout.php">Log Out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
        
          <div class="carousel-item active" style="background: url(assets/img/stue/bgg.jpg);">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown"><span>Stue </span>Coffe</h2>
                <p class="animate__animated animate__fadeInUp"></p>
                <div>
                    <a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

  
	 <!-- ======= Gallery Section ======= -->
     <section id="gallery" class="gallery">
      <div class="container-fluid">

        <div class="section-title">
          <h2>All about <span>Stue Coffe</span></h2>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/stue/1.jpg" class="gallery-lightbox">
                <img src="assets/img/stue/1.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
              <br>
              <p align="justify">Cafe yang sangat nyaman dengan menu yang bermacam-macam, dari minuman sampai makanan. 
                Cafe ini sangat nyaman untuk nongkrong maupun mengerjakan tugas bagi para remaja sekolah ataupun kuliah.
              </p>
              Lokasi : 
              <p align="justify">Jl. Jatiwinangun No.26, Jatiwinangun, Purwokerto Lor, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53114
              </p><h3><p align="center"><b>Stue Coffe :</b></p></h3>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/stue/2.jpg" class="gallery-lightbox">
                <img src="assets/img/stue/2.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Main course-</b></p></h5>
            <table>
              <tr>
                <td>Rice Bowl Korean</td>
                <td>...................................................</td>
                <td>26k</td>
              </tr>
              <tr>
                <td>Crips' Chicken</td>
                <td>...................................................</td>
                <td>28k</td>
              </tr>
              <tr>
                <td>Crips' Dori</td>
                <td>...................................................</td>
                <td>32k</td>
              </tr>
                <td>Indonesian Rendang</td>
                <td>...................................................</td>
                <td>36k</td>
              </tr>
              </tr>
                <td>Chicken Burger</td>
                <td>...................................................</td>
                <td>30k</td>
              </tr>
              </tr>
                <td>Double Chees Burger</td>
                <td>...................................................</td>
                <td>34k</td>
              </tr>
              </tr>
                <td>British Carbonara</td>
                <td>...................................................</td>
                <td>34k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Snack-</b></p></h5>
            <table>
              <tr>
                <td>Potato Wedges</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
              <tr>
                <td>Nacos</td>
                <td>...................................................</td>
                <td>15k</td>
              </tr>
              <tr>
                <td>Tacos</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
                <td>Chicken Pok Pok</td>
                <td>...................................................</td>
                <td>26k</td>
              </tr>
              </tr>
                <td>Chicken Drum Stick</td>
                <td>...................................................</td>
                <td>36k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5><p align="center"><b>-Drink-</b></p></h5>
            <table>
              <tr>
                <td>Kopi Susu Jatwin</td>
                <td>...................................................</td>
                <td>22k</td>
              </tr>
              <tr>
                <td>Palm Sugar Latte</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
              <tr>
                <td>Basoca</td>
                <td>...................................................</td>
                <td>26k</td>
              </tr>
                <td>Caramel Cookies</td>
                <td>...................................................</td>
                <td>28k</td>
              </tr>
              </tr>
                <td>Local Beans</td>
                <td>...................................................</td>
                <td>24k</td>
              </tr>
              </tr>
                <td>Black in Sweet</td>
                <td>...................................................</td>
                <td>28k</td>
              </tr>
            </table>
          </div>
        </div>

        <br>

      </div>
    </section><!-- End Gallery Section -->

    <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
</body>
</html>