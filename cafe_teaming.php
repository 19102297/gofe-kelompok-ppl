<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Te Aming x Auntie</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/icon.jpg" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fixed-top topbar-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start justify-content-between ">
      <i class="bi bi-phone d-flex align-items-center "><span>+082-2272 64240</span></i>
      <i class="bi bi-clock ms-4 d-lg-flex align-items-center me-auto"><span>Tue-Sun: 10:00 AM - 22:00 PM</span></i>
    </div>
  </section>
  <!-- style="position: absolute; right: 5.5em;" -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <div class="logo me-auto">
        <h1><a href="welcome.php" style="font-size: 0.7em;">GoFe</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="welcome.php">Home</a></li>
          <li><a class="nav-link scrollto" href="#menu">Reservasi</a></li>
          <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="nav-link scrollto" href="status.php">Status</a></li>
          <li><a class="nav-link scrollto" href="logout.php">Log Out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">

      <div class="carousel-item active" style="background: url(assets/img/teaming/bgg.jpg);">
        <div class="carousel-container">
          <div class="carousel-content">
            <h2 class="animate__animated animate__fadeInDown"><span>Te Aming x Auntie</span>Cafe</h2>
            <p class="animate__animated animate__fadeInUp"></p>
            <div>
              <a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
          </div>
        </div>
      </div>

    </div>

    </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">


    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container-fluid">

        <div class="section-title">
          <h2>All about <span>Te Aming x Auntie</span></h2>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/teaming/1.jpg" class="gallery-lightbox">
                <img src="assets/img/teaming/1.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <br>
            <p align="justify">Kesan modern sangat kental saat pertama kali memasuki cafe ini.
              Cafe ini menyediakan dua tipe ruangan yang bisa dipilih, ada yang indoor dan ada pula yang outdoor.
            </p>
            Lokasi :
            <p align="justify">Jl. Gn. Muria No.21, Brubahan, Grendeng, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122
            </p>
            <h3>
              <p align="center"><b>Te Aming x Auntie :</b></p>
            </h3>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/teaming/3.jpg" class="gallery-lightbox">
                <img src="assets/img/teaming/3.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        </div>

        <div class="row no-gutters">
          <div class="col-lg-4 col-md-4">
            <h5>
              <p align="center"><b>-Main course-</b></p>
            </h5>
            <table>
              <tr>
                <td>Pepperoni Pizza</td>
                <td>...................................................</td>
                <td>55k</td>
              </tr>
              <tr>
                <td>Ayam Bakar (Paha/Dada/Sayap)</td>
                <td>...................................................</td>
                <td>11k</td>
              </tr>
              <tr>
                <td>Ayam Bakar Kampung (Dada/Paha)</td>
                <td>...................................................</td>
                <td>25k</td>
              </tr>
              <td>Bakmi Goreng</td>
              <td>...................................................</td>
              <td>20k</td>
              </tr>
              </tr>
              <td>Ca Kangkung</td>
              <td>...................................................</td>
              <td>20k</td>
              </tr>
              </tr>
              <td>Nasi Goreng Te Aming</td>
              <td>...................................................</td>
              <td>23k</td>
              </tr>
              </tr>
              <td>Paket Ayam Goreng Rempah</td>
              <td>...................................................</td>
              <td>20k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5>
              <p align="center"><b>-Snack-</b></p>
            </h5>
            <table>
              <tr>
                <td>Korean Taro Bread Mozarella</td>
                <td>...................................................</td>
                <td>20k</td>
              </tr>
              <tr>
                <td>Onion Rings</td>
                <td>...................................................</td>
                <td>17k</td>
              </tr>
              <tr>
                <td>Pisang Bakar (Coklat, Keju)</td>
                <td>...................................................</td>
                <td>15k</td>
              </tr>
              <td>Roti Bakar (Coklat, Keju)</td>
              <td>...................................................</td>
              <td>16k</td>
              </tr>
              </tr>
              <td>Tahu Krispi Cabe Garam<< /td>
              <td>...................................................</td>
              <td>16k</td>
              </tr>
            </table>
          </div>
          <div class="col-lg-4 col-md-4">
            <h5>
              <p align="center"><b>-Drink-</b></p>
            </h5>
            <table>
              <tr>
                <td>Alpen Candy</td>
                <td>...................................................</td>
                <td>23k</td>
              </tr>
              <tr>
                <td>Caramel Cookie Corn</td>
                <td>...................................................</td>
                <td>23k</td>
              </tr>
              <tr>
                <td>Peppermint Lemon</td>
                <td>...................................................</td>
                <td>33k</td>
              </tr>
              <td>Creamy Hazelnut</td>
              <td>...................................................</td>
              <td>23k</td>
              </tr>
              </tr>
              <td>Sweet Auntie</td>
              <td>...................................................</td>
              <td>21k</td>
              </tr>
              </tr>
              <td>Vanila Brown</td>
              <td>...................................................</td>
              <td>22k</td>
              </tr>
            </table>
          </div>
        </div>

        <br>

      </div>
    </section><!-- End Gallery Section -->

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>
</body>

</html>