<?php
// Initialize the session
require('config.php');
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
  header("location: login.php");
  exit;
}

if (isset($_SESSION['id'])) {
  $sql = "SELECT *
            FROM users
            WHERE user_id =" . $_SESSION['id'];
  $query = mysqli_query($link, $sql);
  if ($query) {
    while ($row = mysqli_fetch_array($query)) {
      $myRole = $row['role'];
    }
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Welcome to GoFe</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/icon.jpg" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fixed-top topbar-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-center justify-content-lg-start justify-content-between ">
      <i class="bi bi-phone d-flex align-items-center "><span>+082-2272 64240</span></i>
      <i class="bi bi-clock ms-4 d-lg-flex align-items-center me-auto"><span>Tue-Sun: 10:00 AM - 22:00 PM</span></i>
    </div>
  </section>
  <!-- style="position: absolute; right: 5.5em;" -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <div class="logo me-auto">
        <h1><a href="welcome.php" style="font-size: 0.7em;">GoFe</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="welcome.php">Home</a></li>
          <li><a class="nav-link scrollto" href="#menu">Reservasi</a></li>
          <li><a class="nav-link scrollto" href="#gallery">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="nav-link scrollto" href="status.php">Status</a></li>
          <li><a class="nav-link scrollto" href="logout.php">Log Out</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
      <?php if ($myRole == 'admin') {
        echo '<a  class = "book-a-table-btn bi ms-4 d-lg-flex d-none" href="index.php" style="background: #DC143C;" >Admin Site</a>';
      } ?>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">

      <div class="carousel-item active" style="background: url(assets/img/logo.png);">
        <div class="carousel-container">
          <div class="carousel-content">
            <h2 class="animate__animated animate__fadeInDown"><span>Golet</span>Cafe</h2>
            <h5 style="color:white;">START YOUR DAY WITH GREAT TASTE </h5>
            <p class="animate__animated animate__fadeInUp"></p>
          </div>
        </div>
      </div>

    </div>

    </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">


    <!-- ======= Menu Section ======= -->
    <section id="menu" class="menu">
      <div class="container">

        <div class="section-title">
          <h2>Pilih <span>Cafe</span></h2>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="menu-flters">

          </div>
        </div>

        <div class="row menu-container">

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_society.php">Society Cafe</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Fried Dumpling, Onion Rings
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_teaming.php">Teaming X Aunti</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Korean Taro Bread Mozarella
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_taman.php">Taman Coffe</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Chicken Java Sauce. Tuna Salad
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_massapi.php">Massapi Cafe & Resto</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Sapi Sambal Ijo. Pecak Sapi
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_omni.php">Madang x Omni</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Omni Thai Tea. Vanilla Twist
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_thesoeds.php">The Soeds Cafe</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Crispy Duck. Siganture Bakmi
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-dessert">
            <div class="menu-content">
              <a href="cafe_sekawan.php">Sekawan Social Space</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Nasi Ayam Mentega. Nasi Telur Lyfe
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_stue.php">Stue Coffe</a><a href="bookingform.php" class="btn-book animate__animated animate__fadeInUp scrollto">Reservasi</a>
            </div>
            <div class="menu-ingredients">
              Chicken Katsu Sambal Matah. Nasi Goreng
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Menu Section -->


    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container-fluid">

        <div class="section-title">
          <h2>Some photos from <span>Cafe PWT</span></h2>
          <p></p>
        </div>

        <div class="row no-gutters">

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z1.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z1.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z2.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z2.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z3.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z3.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z4.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z4.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z11.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z11.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z6.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z6.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z7.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z7.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="assets/img/gallery/z8.jpg" class="gallery-lightbox">
                <img src="assets/img/gallery/z8.jpg" alt="" class="img-fluid">
              </a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Gallery Section -->





    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Contact <span>Cafe</span></h2>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="menu-flters">

          </div>
        </div>

        <div class="row menu-container">

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_society.php">Society Cafe</a><span></span>
            </div>
            <div class="menu-ingredients">
              Jl. Prof. Dr.Suharso Kelurahan No.9B, Arcawinangun, Kec. Purwokerto Timur.No HP 0811-2821-168
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_teaming.php">Te Aming x Auntie</a><span></span>
            </div>
            <div class="menu-ingredients">
              Jl. Gn. Muria No.21, Brubahan, Grendeng, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_taman.php">Taman Coffe</a><span></span>
            </div>
            <div class="menu-ingredients">
              Jl. Gn. Muria No.985, Karang Bawang, Grendeng, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_massapi.php">Massapi Cafe & Resto</a><span></span>
            </div>
            <div class="menu-ingredients">
              Dusun III Berubahan, Kemutug Lor, Kec. Baturaden, Kabupaten Banyumas.No HP 0815-7563-5556
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_omni.php">Madang x Omni</a><span></span>
            </div>
            <div class="menu-ingredients">
              Jl. Kolonel Sugiono No.15 C, Tipar, Purwanegara, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53116
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-foods">
            <div class="menu-content">
              <a href="cafe_thesoeds.php">The Soeds Cafe</a><span></span>
            </div>
            <div class="menu-ingredients">
              Jl. Prof. Dr. Suharso Kelurahan No.9B, Arcawinangun, Kec. Purwokerto Timur, Kabupaten Banyumas.
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-dessert">
            <div class="menu-content">
              <a href="cafe_sekawan.php">Sekawan Social Space</a><span></span>
            </div>
            <div class="menu-ingredients">
            Jl. Stadion Mini 1 No.1, Karangbawang, Purwokerto Kulon, Kec. Purwokerto Sel., Kabupaten Banyumas, Jawa Tengah 53141
            </div>
          </div>

          <div class="col-lg-6 menu-item filter-drinks">
            <div class="menu-content">
              <a href="cafe_stue.php">Stue Coffe</a><span></span>
            </div>
            <div class="menu-ingredients">
            Jl. Jatiwinangun No.26, Jatiwinangun, Purwokerto Lor, Kec. Purwokerto Tim., Kabupaten Banyumas, Jawa Tengah 53114
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>GoFe</h3>
      <div class="social-links">
        <a href="https://www.facebook.com/themuffinhouse/" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="https://www.instagram.com/themuffinhouses2/" class="instagram"><i class="bx bxl-instagram"></i></a>
      </div>

    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>